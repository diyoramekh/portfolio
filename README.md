# Diyora Mekhmonova

## Software Engineer | Frontend Developer

**Contact Information:**

- Email: [diyoramex@gmail.com](mailto:diyoramex@gmail.com)
- LinkedIn: [linkedin.com/in/diyoramekh](https://linkedin.com/in/diyoramekh)
- GitLab: [gitlab.com/diyoramekh](https://gitlab.com/diyoramekh)

---

## About Me

Frontend developer with 3 years of experience specializing in React.js. Skilled in building responsive and user-friendly web applications.

---

## Work Experience

### Frontend Developer

**Udevs, Tashkent, Uzbekistan**  
_January 2023 - April 2024_

- Collaborated with project managers, designers, and backend developers to deliver high-quality and visually appealing web applications.
- Participated and conducted in Agile development processes, including sprint planning, daily stand-ups, and retrospectives.
- Wrote clean, maintainable, and efficient code for frontend components using modern frameworks such as React, Next.js, or Vue.js.

### Frontend Developer

**OKS Technologies, Tashkent, Uzbekistan**  
_June 2021 - December 2022_

- Contributed to the development and maintenance of dynamic web applications using HTML, CSS, JavaScript, and modern frameworks like React.js.
- Worked closely with backend developers to integrate RESTful APIs, ensuring smooth data flow and dynamic content rendering.
- Identified and resolved front-end issues, ensuring high performance and compatibility across different platforms.

---

## Skills

**Hard Skills:**

- HTML5, CSS3, Sass, JavaScript, TypeScript, React.js, Vue.js, Next.js, Nest.js, Redux, Media Queries, Tailwind CSS, RESTful API, Jest, SEO, Git, Docker

**Soft Skills:**

- Agile Methodologies, Scrum, Kanban, Time management, Team management, Team leading, Problem solving

---

## Projects

### Game Drive

Social media platform for gamers

- Developed a social media platform for gamers using TypeScript, React, TanStack Query, React Hook Form, and Sass.
- Implemented user-friendly interfaces and complex form handling with React Hook Form.
- Utilized TanStack Query for efficient data fetching, caching, and synchronization.
- Styled the application with Sass to create a visually appealing and consistent design.
- Worked in an Agile environment, ensuring timely delivery of features and enhancements.

### Paynet Avia

Airline tickets booking platform

- Developed a dynamic airline tickets booking platform using Vue.js, JavaScript, SCSS, and Sass.
- Created responsive and interactive user interfaces to enhance user experience.
- Ensured seamless integration of APIs for real-time data fetching and booking processes.
- Styled the application using SCSS and Sass to maintain a modular and maintainable codebase.
- Collaborated with backend developers and participated in Agile processes for efficient project management.

---

## Languages

- **English:** Professional (IELTS 6.5)
- **Russian:** Professional
- **Uzbek:** Native
