export default function Hero() {
  return (
    <div className="grid lg:grid-cols-2 lg:grid-rows-2">
      <div className="lg:h-80 p-10 bg-primary">
        <h1 className="xl:text-7xl md:text-5xl text-3xl text-black">
          Diyora Mekhmonova
        </h1>
        <h1 className="xl:text-7xl md:text-5xl text-3xl text-black lg:hidden md:mt-4">
          Frontend Developer <br />
          <span className="xl:text-5xl md:text-2xl text-lg text-black">
            with 3 years of experience
          </span>
        </h1>
      </div>
      <div className="w-full items-center justify-center lg:flex hidden"></div>
      <div className="h-80 p-10 lg:flex hidden items-end justify-end"></div>
      <div className="h-80 p-10 bg-primary lg:flex hidden">
        <h1 className="xl:text-7xl md:text-5xl text-black">
          Frontend Developer <br />
          <span className="xl:text-5xl md:text-2xl text-black">
            with 3 years of experience
          </span>
        </h1>
      </div>
    </div>
  );
}
