import Link from "next/link";

export default function Header() {
  return (
    <header className="grid sticky top-0 z-30 backdrop-filter backdrop-blur-lg lg:grid-cols-3 grid-cols-1 px-10 border-b border-borderc w-full items-center justify-center">
      <div className="lg:flex hidden" />
      <nav className="border-x xl:text-lg text-sm border-borderc px-6 py-4 flex flex-row justify-between">
        <Link href={"/"} className="hover:underline hover:text-primary">
          Home
        </Link>
        <Link href={"#skills"} className="hover:underline hover:text-primary">
          Skills
        </Link>
        <Link href={"#projects"} className="hover:underline hover:text-primary">
          Projects
        </Link>
        <Link href={"#contact"} className="hover:underline hover:text-primary">
          Contact
        </Link>
      </nav>
    </header>
  );
}
