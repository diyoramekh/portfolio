import ProjectCard from "./ProjectCard";
import { projects } from "@/data/projects";

export default function Projects() {
  return (
    <section id="projects" className="grid grid-cols-12">
      <article className="col-span-1 border-r border-t border-b border-borderc" />
      <article className="col-span-11">
        {projects?.map((p) => (
          <ProjectCard
            key={p.id}
            name={p.name}
            category={p.category}
            techStack={p.techStack}
            url={p.url}
            imgUrl={p.imgUrl}
            isLast={p.isLast}
          />
        ))}
      </article>
    </section>
  );
}
