import Arrow from "@/icons/Arrow";
import Image from "next/image";
import Link from "next/link";

export default function ProjectCard({
  name,
  category,
  techStack,
  url,
  imgUrl,
  isLast,
}) {
  return (
    <div
      className={`border-t border-borderc lg:px-20 px-6 lg:py-14 py-6 grid md:grid-cols-2 md:gap-12 gap-4 ${
        isLast ? "border-b" : ""
      }`}
    >
      <Link href={url} passHref={true} target="_blank">
        <div className="lg:h-[320px] md:h-[160px] h-[150px] overflow-hidden hover:opacity-75 cursor-pointer">
          <Image src={imgUrl} width={600} height={300} alt="project" />
        </div>
      </Link>

      <div className="flex flex-col justify-between h-full">
        <div className="flex flex-row justify-between">
          <div className="flex flex-col gap-2">
            <Link href={url} passHref={true} target="_blank">
              <h3 className="lg:text-5xl text-2xl hover:underline hover:text-primary">
                {name}
              </h3>
            </Link>
            <p className="lg:text-xl">{category}</p>
          </div>
          <Link href={url} passHref={true} target="_blank">
            <Arrow />
          </Link>
        </div>

        <div className="lg:text-xl md:pt-0 pt-4 text-sm grid grid-cols-2">
          {techStack?.map((t) => (
            <p key={t}>{t}</p>
          ))}
        </div>
      </div>
    </div>
  );
}
