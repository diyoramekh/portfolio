import Image from "next/image";

export default function Location() {
  return (
    <div className="border flex flex-row items-center gap-8 border-borderc border-l-0 rounded-r-full w-max md:py-4 py-3 md:px-8 px-4">
      <h1 className="md:text-xl text-sm">
        based in <br /> Uzbekistan
      </h1>
      <div className="w-10 h-10 bg-white rounded-full p-1 ">
        <Image src={"/globe.png"} width={50} height={50} alt="earth" />
      </div>
    </div>
  );
}
