export default function Skills() {
  return (
    <section
      id="skills"
      className="grid grid-rows-3 grid-cols-12 border-y border-borderc"
    >
      <div className="row-span-3 col-span-1 border-r border-borderc"></div>
      <div className="col-span-11 grid md:grid-cols-2 md:p-10 p-4 border-b border-borderc">
        <div />
        <div className="flex flex-col md:gap-4 gap-2">
          <h2 className="xl:text-7xl md:text-5xl text-2xl text-primary">
            About
          </h2>
          <p className="lg:text-lg text-sm">
            Frontend developer skilled in React.js, building responsive,
            user-friendly web applications. Experienced in Agile methodologies.
            Studying Computer Science at University Malaysia of Computer Science
            and Engineering (UNIMY).
          </p>
        </div>
      </div>
      <div className="row-span-2 col-span-11 grid md:grid-cols-2 md:pt-10 pt-4 md:px-10 px-4 md:pb-0 pb-4">
        <h2 className="xl:text-7xl md:text-5xl text-2xl text-primary md:h-auto h-2">
          Skills
        </h2>
        <div className="grid grid-cols-2 md:grid-cols-2 lg:grid-row-5 md: grid-row-4  gap-4 lg:text-2xl">
          <p>HTML</p>
          <p>CSS</p>
          <p>JavaScript</p>
          <p>TypeScript</p>
          <p>React</p>
          <p>Redux</p>
          <p>Next.js</p>
          <p>Vue.js</p>
          <p>Nest.js</p>
          <p>SEO</p>
          <p>Tailwind CSS</p>
          <p>MUI</p>
        </div>
      </div>
    </section>
  );
}
