import Link from "next/link";

export default function Contact() {
  return (
    <section
      id="contact"
      className="flex flex-col lg:my-44 lg:mx-72 md:mx-20 mx-8 md:gap-10 md:my-0 my-20 gap-4"
    >
      <div className="md:p-10 py-4 border-b border-borderc">
        <h2 className="xl:text-8xl md:text-5xl text-2xl">
          Let&apos;s work <br /> together
        </h2>
      </div>
      <div className="flex flex-row gap-4 md:text-xl text-sm justify-end">
        <Link
          className="hover:text-primary hover:underline"
          href={"https://mailto:diyoramex@gmail.com"}
          passHref={true}
          target="_blank"
        >
          Mail
        </Link>
        <Link
          className="hover:text-primary hover:underline"
          href={"https://www.linkedin.com/in/diyoramekh/"}
          passHref={true}
          target="_blank"
        >
          LinkedIn
        </Link>
        <Link
          className="hover:text-primary hover:underline"
          href={"https://gitlab.com/diyoramekh"}
          passHref={true}
          target="_blank"
        >
          Gitlab
        </Link>
      </div>
    </section>
  );
}
