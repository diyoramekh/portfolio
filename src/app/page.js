import Contact from "../components/Contact";
import Header from "../components/Header";
import Hero from "../components/Hero";
import Location from "../components/Location";
import Projects from "../components/Projects";
import Skills from "../components/Skills";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-centern bg-back md:gap-20 gap-10">
      <Header />
      <Hero />
      <Location />
      <Skills />
      <Projects />
      <Contact />
      <div />
    </main>
  );
}
