import Head from "next/head";
import "./globals.css";

import { Inter } from "next/font/google";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Portfolio",
  description: "portfolio",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <Head>{/* <link rel="icon" href="/favicon.ico" sizes="any" /> */}</Head>
      <body className={inter.className}>{children}</body>
    </html>
  );
}
