export const projects = [
  {
    id: 4,
    name: "GameDrive",
    category: "Social Media",
    techStack: [
      "TypeScript",
      "TanStack Query",
      "React.js",
      "SASS",
      "Redux",
      "SCSS",
      "React Hook Form",
    ],
    url: "http://gamedev.ucode.run/auth/sign-in",
    imgUrl: "/gamedrive.png",
  },
  {
    id: 3,
    name: "Paynet Avia",
    category: "Airline tickets",
    techStack: ["JavaScript", "SCSS", "Vue.js", "SASS"],
    url: "https://en.paynet.uz/avia",
    imgUrl: "/paynet.png",
  },
  {
    id: 5,
    name: "Snowflake",
    category: "Relaxation",
    techStack: ["HTML", "CSS"],
    url: "https://remarkable-cuchufli-841ecb.netlify.app/",
    imgUrl: "/snowf.png",
    isLast: true,
  },
  {
    id: 6,
    name: "Water Wave",
    category: "Relaxation",
    techStack: ["HTML", "CSS"],
    url: "https://super-sawine-f549c7.netlify.app/",
    imgUrl: "/water.png",
  },
  {
    id: 1,
    name: "IeltsCoach",
    category: "Education",
    techStack: [
      "JavaScript",
      "ContextAPI",
      "Next.js",
      "Telegram API",
      "Nest.js",
      "OpenAI",
      "Tailwind CSS",
    ],
    url: "https://app.ieltscoach.uz/",
    imgUrl: "/ieltsapp.png",
  },
  {
    id: 2,
    name: "IeltsCoach",
    category: "Education",
    techStack: ["JavaScript", "Tailwind CSS", "Next.js", "SEO"],
    url: "https://ieltscoach.uz/",
    imgUrl: "/ieltslanding.png",
  },
];
